package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/slightlykiwi-oss/git-for/cmdline"
	"gitlab.com/slightlykiwi-oss/git-for/console"
	"gitlab.com/slightlykiwi-oss/git-for/project"
)

var csl console.Console

func main() {
	cmdline.CheckHelpRequested()

	opts, err := cmdline.ReadArguments(os.Args[1:]) // First argument is the name of the executable, so we remove.
	if err != nil {
		fmt.Println(err)
		cmdline.PrintHelpAndExit(1)
	}

	csl = console.New(opts)

	project := project.New(csl)
	template := readTemplate(project, opts)

	applyTemplate(project, template)
}

func readTemplate(rdr project.Reader, opts cmdline.Options) project.Template {
	csl.Info("Searching for " + opts.Template)
	defer csl.TimeTrack(time.Now(), "Finding "+opts.Template)

	template, err := rdr.Read(opts.Template)
	if err != nil {
		fatal(err)
	}

	return template
}

func applyTemplate(proj project.Project, template project.Template) {
	csl.Info("Applying template to repository")
	defer csl.TimeTrack(time.Now(), "Applying template")

	writeFiles(proj, template)
	configRepo(proj, template.Configs)
	execCommands(proj, template.Commands)
}

func writeFiles(wrtr project.Writer, template project.Template) {
	err := wrtr.Write(template)
	if err != nil {
		fatal(err)
	}
}

func configRepo(cfg project.Configurator, configs []project.Config) {
	err := cfg.Configure(configs)
	if err != nil {
		fatal(err)
	}
}

func execCommands(exe project.Executor, cmds []string) {
	err := exe.Execute(cmds)
	if err != nil {
		fatal(err)
	}
}

func fatal(err error) {
	fmt.Println("FATAL ERROR - " + err.Error())
	os.Exit(1)
}
