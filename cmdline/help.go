package cmdline

import (
	"fmt"
	"os"
)

const helpMessage = "git for [template] [-v:optional]\n\n" +
	"	[template] : the name of the template to use to configure the git repository.\n" +
	"	[-v] : (optional) verbose output.\n\n" +
	"   Type git for -h to see this message again."

// CheckHelpRequested short circuits program execution if -h flag is the first command line argument.
func CheckHelpRequested() {
	argLen := len(os.Args)
	if argLen > 1 && os.Args[1] == "-h" {
		PrintHelpAndExit(0)
	}
}

// PrintHelpAndExit prints the help text and exits the program with the specified exitCode
func PrintHelpAndExit(exitCode int) {
	fmt.Println(helpMessage)
	os.Exit(0)
}
