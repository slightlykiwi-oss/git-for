package cmdline

import (
	"reflect"
	"testing"
)

func TestReadArguments(t *testing.T) {
	type test struct {
		name    string
		args    []string
		want    Options
		wantErr bool
	}

	tests := []test{
		test{"TestNoArgs", []string{}, Options{}, true},
		test{"TestTemplateOnly", []string{"potato"}, Options{"potato", false}, false},
		test{"TestVerboseOnly", []string{"-v"}, Options{}, true},
		test{"TestTemplateAndVerbose", []string{"fromage", "-v"}, Options{"fromage", true}, false},
		test{"TestUnknownFlags", []string{"yolo", "-x", "-y", "-z"}, Options{"yolo", false}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadArguments(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadArguments() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadArguments() = %v, want %v", got, tt.want)
			}
		})
	}
}
