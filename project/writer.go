package project

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

const gitIgnoreFile = ".gitignore"
const gitAttributesFile = ".gitattributes"
const gitConfigFile = ".git/config"

const appliedHeader = "## Applied by git-for\n\n"

// Writer exposes methods for writing the git files as specified by a template
type Writer interface {
	Write(template Template) error
}

type writer struct {
	fileExists    func(path string) bool
	getWorkingDir func() (string, error)
	readFile      func(path string) ([]byte, error)
	csl           console.Console
}

// createWriter constructs a new instance of writer with actual methods
func createWriter(csl console.Console) writer {
	return writer{fileExists, os.Getwd, ioutil.ReadFile, csl}
}

func (wrtr writer) Write(template Template) error {
	if !wrtr.fileExists(".git") {
		return fmt.Errorf("Not in the root of a git repository. Please call from the root of your repo")
	}
	wrtr.csl.Info("Writing settings files...")
	defer wrtr.csl.TimeTrack(time.Now(), "Writing files")

	gitignorePath, err := wrtr.getFileInWd(gitIgnoreFile)
	if err != nil {
		return err
	}

	wrtr.csl.Info("Writing .gitignore file...")
	err = wrtr.writeLines(gitignorePath, template.Ignores)
	if err != nil {
		return err
	}

	wrtr.csl.Info("Writing .gitattributes file...")
	err = wrtr.writeLines(gitAttributesFile, template.Attributes)
	if err != nil {
		return err
	}

	return nil
}

func (wrtr writer) writeLines(path string, lines []string) error {
	wrtr.csl.Verbose("Writing to " + path)
	defer wrtr.csl.TimeTrackIfVerbose(time.Now(), "Writing")

	existingText, err := wrtr.readIfExists(path)
	if err != nil {
		return err
	}

	content, err := wrtr.mergeLines(existingText, lines)
	if err != nil {
		return err
	}

	return wrtr.overwrite(path, []byte(content))
}

func (wrtr writer) writeConfigs(path string, configs []Config) error {
	wrtr.csl.Verbose("Writing to " + path)
	defer wrtr.csl.TimeTrackIfVerbose(time.Now(), "Writing")

	existingText, err := wrtr.readIfExists(path)
	if err != nil {
		return err
	}

	content, err := wrtr.mergeConfigs(existingText, configs)
	if err != nil {
		return err
	}

	return wrtr.overwrite(path, []byte(content))
}

func (wrtr writer) readIfExists(path string) (string, error) {
	existingText := ""
	if wrtr.fileExists(path) {
		existing, err := wrtr.readFile(path)
		if err != nil {
			return "", err
		}

		existingText = string(existing)
		wrtr.csl.Verbose("Merged!")
	}

	return existingText, nil
}

func (wrtr writer) overwrite(path string, content []byte) error {
	wrtr.csl.Verbose("Disk operation -> write to " + path)
	defer wrtr.csl.TimeTrackIfVerbose(time.Now(), "Disk operation")

	return ioutil.WriteFile(path, content, 0664)
}

func (wrtr writer) mergeLines(existing string, lines []string) (string, error) {
	wrtr.csl.Verbose("Merging lines...")
	defer wrtr.csl.TimeTrackIfVerbose(time.Now(), "Merge")
	merged := existing

	wrtr.removeLines(&merged, lines)

	merged += appliedHeader
	merged += strings.Join(lines, "\n")

	return merged, nil
}

func (wrtr writer) mergeConfigs(existing string, configs []Config) (string, error) {
	return "", fmt.Errorf("Not implemented")
}

func (wrtr writer) removeLines(current *string, linesToRemove []string) error {
	wrtr.csl.Verbose("Removing duplicates...")
	defer wrtr.csl.TimeTrackIfVerbose(time.Now(), "Duplicate removal")
	rdr := bufio.NewReader(strings.NewReader(*current))

	resultLines := []string{}
	var err error
	err = nil
	for err == nil {
		var curLine string
		curLine, err = rdr.ReadString('\n')
		if err != nil && err != io.EOF {
			return err
		}

		found := false
		for j := 0; j < len(linesToRemove); j++ {
			lineToRemove := linesToRemove[j]
			if lineToRemove == curLine || lineToRemove+"\n" == curLine {
				found = true
				break
			}
		}

		if !found {
			resultLines = append(resultLines, curLine)
		}
	}

	*current = strings.Join(resultLines, "")

	return nil
}

func (wrtr writer) getFileInWd(file string) (string, error) {
	wrtr.csl.Verbose("Locating file in working directory...")
	defer wrtr.csl.TimeTrackIfVerbose(time.Now(), "Locating file")
	wd, err := wrtr.getWorkingDir()
	if err != nil {
		return wd, err
	}

	return wd + "/" + file, nil
}
