package project

import (
	"fmt"
	"time"

	"os/exec"

	"strings"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

// Configurator exposes methods for configuring a repo based on a template file
type Configurator interface {
	Configure(configs []Config) error
}

// configurator holds mockable methods and objects
type configurator struct {
	fileExists func(path string) bool
	runCommand func(*exec.Cmd) (string, string, error)
	csl        console.Console
}

// createConfigurator constructs a new instance of configurator with actual methods
func createConfigurator(csl console.Console) configurator {
	return configurator{fileExists, runCommand, csl}
}

func (cfg configurator) Configure(configs []Config) error {
	if !cfg.fileExists(".git") {
		return fmt.Errorf("Not in the root of a git repository. Please call from the root of your repo")
	}
	cfg.csl.Info("Configuring repository...")
	defer cfg.csl.TimeTrack(time.Now(), "Repository configuration")

	commands, err := cfg.formatCommands(configs)
	if err != nil {
		return err
	}

	for _, command := range commands {
		cfg.csl.Verbose("Running " + command.Path + " " + strings.Join(command.Args, " "))

		outbuf, errbuf, err := cfg.runCommand(command)
		if err != nil {
			return err
		}

		if outbuf != "" {
			cfg.csl.Verbose("Output:\n" + outbuf)
		}
		if errbuf != "" {
			cfg.csl.Verbose("Errors:\n" + errbuf)
		}
	}

	return nil
}

func (cfg configurator) formatCommands(configs []Config) ([]*exec.Cmd, error) {
	result := []*exec.Cmd{}

	for _, config := range configs {
		config.Key = strings.TrimSpace(config.Key)
		config.Value = strings.TrimSpace(config.Value)
		if config.Key == "" || config.Value == "" {
			return result, fmt.Errorf("Missing config key or value. Please check your template file")
		}

		command := exec.Command("git", "config", config.Key, config.Value)
		result = append(result, command)
	}

	return result, nil
}
