package project

import (
	"gitlab.com/slightlykiwi-oss/git-for/console"
)

// Template represents a project template
type Template struct {
	Ignores    []string `json:"ignores"`
	Configs    []Config `json:"configs"`
	Attributes []string `json:"attributes"`
	Commands   []string `json:"commands"`
}

// Config represents a project config section
type Config struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Project holds the template reader and the git file writers
type Project struct {
	Reader
	Writer
	Configurator
	Executor
	csl console.Console
}

// New creates an instance of Project
func New(csl console.Console) Project {
	rdr := createReader(csl)
	wrtr := createWriter(csl)
	cfg := createConfigurator(csl)
	exe := createExecutor(csl)
	return Project{rdr, wrtr, cfg, exe, csl}
}
