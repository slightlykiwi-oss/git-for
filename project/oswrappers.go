package project

import (
	"bytes"
	"os"
	"os/exec"
)

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func runCommand(command *exec.Cmd) (string, string, error) {
	var outbuf bytes.Buffer
	var errbuf bytes.Buffer
	command.Stdout = &outbuf
	command.Stderr = &errbuf

	err := command.Run()
	return outbuf.String(), outbuf.String(), err
}
