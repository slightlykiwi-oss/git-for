package project

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"time"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

// Reader exposes methods for reading a template file
type Reader interface {
	Read(name string) (Template, error)
}

// reader holds mockable methods
type reader struct {
	fileExists func(path string) bool
	getEnvVar  func(key string) string
	readFile   func(path string) ([]byte, error)
	csl        console.Console
}

const gitforEnvVar = "GITFOR"
const builtInDir = "/builtin/"
const customDir = "/custom/"

// createReader constructs a new instance of Reader with actual methods
func createReader(csl console.Console) reader {
	return reader{fileExists, os.Getenv, ioutil.ReadFile, csl}
}

// Read looks for the specified file first in the $GITFOR/builtin directory
// then in $GITFOR/custom and returns a Template
func (rdr reader) Read(name string) (Template, error) {
	rdr.csl.Info("Reading " + name + " template...")
	defer rdr.csl.TimeTrack(time.Now(), "Reading template")
	filepath, err := rdr.findTemplateFile(name)

	template := Template{}
	if err == nil {
		template, err = rdr.read(filepath)
	}

	return template, err
}

func (rdr reader) read(path string) (Template, error) {
	rdr.csl.Verbose("Reading file " + path + " ...")
	defer rdr.csl.TimeTrackIfVerbose(time.Now(), "Reading file")
	raw, err := rdr.readFile(path)
	if err != nil {
		return Template{}, err
	}

	if len(raw) < 1 {
		return Template{}, fmt.Errorf("Template file appears to be empty")
	}

	var template Template
	err = json.Unmarshal(raw, &template)
	if err != nil {
		return Template{}, err
	}

	return template, nil
}

func (rdr reader) findTemplateFile(name string) (string, error) {
	rdr.csl.Verbose("Looking for template " + name + " ...")
	defer rdr.csl.TimeTrackIfVerbose(time.Now(), "Finding template")

	gitforDir := rdr.getEnvVar(gitforEnvVar)
	rdr.csl.Verbose("Looking in " + gitforDir)

	if gitforDir == "" {
		return "", fmt.Errorf("GITFOR Environment variable not set.\nPlease reinstall git-for or set it manually")
	}

	filename := name + ".json"

	builtInPath := gitforDir + builtInDir + filename
	customPath := gitforDir + customDir + filename
	if rdr.fileExists(builtInPath) {
		rdr.csl.Verbose("Found built-in template: " + builtInPath)
		return builtInPath, nil
	} else if rdr.fileExists(customPath) {
		rdr.csl.Verbose("Found custom template: " + builtInPath)
		return customPath, nil
	} else {
		return "", fmt.Errorf("Could not find any template named \"" + name + "\"")
	}
}
