package project

import (
	"os/exec"
	"reflect"
	"testing"

	"gitlab.com/slightlykiwi-oss/git-for/console"
)

func Test_executor_formatCommands(t *testing.T) {
	type test struct {
		name    string
		exe     executor
		cmds    []string
		want    []*exec.Cmd
		wantErr bool
	}

	mockExecutor := executor{nil, nil, console.Console{}}
	expectedCmd := exec.Command("git")
	expectedCmd.Args = []string{"git", "status"}

	tests := []test{
		test{"Command", mockExecutor, []string{"git status"}, []*exec.Cmd{expectedCmd}, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.exe.formatCommands(tt.cmds)
			if (err != nil) != tt.wantErr {
				t.Errorf("executor.formatCommands() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("executor.formatCommands() = %v, want %v", got, tt.want)
			}
		})
	}
}
